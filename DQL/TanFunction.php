<?php

namespace Spine\TrigonometricDQLBundle\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

/**
 *  CosFunction ::= "TAN" "(" ArithmeticPrimary ")"
 */
class TanFunction extends FunctionNode
{
    public $angleValue = null;

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);

        $this->angleValue = $parser->SimpleArithmeticExpression();

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return 'TAN(' . $sqlWalker->walkSimpleArithmeticExpression(
            $this->angleValue
        ) . ')';
    }
}
