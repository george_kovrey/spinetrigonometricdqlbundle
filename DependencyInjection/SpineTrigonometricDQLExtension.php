<?php

namespace Spine\TrigonometricDQLBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;

class SpineTrigonometricDQLExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    public function prepend(ContainerBuilder $container)
    {
        $config = [
            'orm' => [
                'dql' => [
                    'numeric_functions' => [
                        'sin' => 'Spine\TrigonometricDQLBundle\DQL\SinFunction',
                        'cos' => 'Spine\TrigonometricDQLBundle\DQL\CosFunction',
                        'tan' => 'Spine\TrigonometricDQLBundle\DQL\TanFunction',
                        'cot' => 'Spine\TrigonometricDQLBundle\DQL\CotFunction',
                        'pi'  => 'Spine\TrigonometricDQLBundle\DQL\PiFunction',
                    ]
                ]
            ]
        ];
        $container->prependExtensionConfig('doctrine', $config);
    }
}
