<?php

namespace Spine\TrigonometricDQLBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $tree = new TreeBuilder();
        $tree->root('spine_trigonometric_dql')
            ->end();
        return $tree;
    }
}
